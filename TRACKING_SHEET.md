PROJECT n°17  
Weekly Tracking Sheet
=====================

25/01
---------
First session :   
* Definition of the project's limits and perimeter :

The aim is first to suggest an Open API specification adapted to OAR.  
Then, we shall start implementing it.  

* Proofs of concept (method or idea to demonstrate the feasibility of a concept), tutorials, complete documentations are expected.  

* [Swagger Petstore](https://editor.swagger.io/) 's Json is obtained and studied

01/02
------------
* [Open API Specification](http://spec.openapis.org/oas/v3.0.3#version-3-0-3) : what is it and how to build one ?
* [The Kubernetes' API](https://kubernetes.io/docs/concepts/overview/kubernetes-api/#api-specification)
* creation of the _code_ repository, the *README.md* and this tracking sheet

08/02
-------------
* Ongoing comprehension of the [Open API Specification](http://spec.openapis.org/oas/v3.0.3#version-3-0-3)
* New way of building a RestAPI : [FlaskRestX](https://flask-restx.readthedocs.io/en/latest/)
* Copy of the whole OAR RestAPI's folder in *code* repository and creation of a *new_rest_api* folder to insert our code  

-> TODO : when FlaskRestX is well handled, get inspired by OAR current Rest API code to start coding our own

22/02
----------------
* Study of the current code of OAR's OpenApi (folder *oar/rest_api* in *code* repository)
* Definition of the Rest API's structure (first requests) adapted to OAR (in *oar/new_rest_api*)  

-> TODO : definition of the remaining methods, before filling them in

01/03
----------------
* Ongoing definition of job- and media- requests in related files
* Clarification of the next steps (definition of the skeleton, creation of fake resulting json files, test of the skeleton)

-> TODO : implement each family of requests in an independant file, then get each implementation back in api.py file (needs to understand how classes can be imported in Python)  
-> TODO : if explications can't be found on the Web, directly ask the question on Stack Overflow or another forum (github issues for example)

02/03
-----------------
* Ongoing definition of job-, colmet-, config- and admission_rules- requests in their related files  
* Creation of a fake api response to try the test_app_frontend_index test -> fail to pass  
* Implementation of jobs' model to match the OAR database documentation with a maximal  accuracy  
* Implementation of a simple DAO for jobs in order to experiment masks and try to implement requests based on locally generated fake objects   
   
-> TODO : make the test a success  

08/03
------------------
* Improvement of data models in *new_rest_api/views*
* Comprehension and implementation of the authentication feature in *job.py*
* **test_app_frontend_index**, **test_app_frontend_version** and **test_app_frontend_timezone** in *test_views.py* passed with a real implementation of the "frontend endpoint".  

-> TODO : understand the auth process and how we could make it work then extend the tests  

09/03  
---------------------
* Merge and debugging of every requests' files into *api.py*  
* Addition of authorization's documentation for each request  
* Frontend tests passed and Media tests ongoing  

-> TODO : keep testing requests and understanding the auth process  

22/03  
---------------------    
* Understanding of the Request Parser of Flask RestX, in order to use input parameters  
* Implementation of the requests' functions for *frontend.py* and *resource.py*  
* Skeleton of *stress_factor.py*   

-> TODO : keep implementing inner functions, then test the api  

23/03  
----------------------   
* Clarification about how to use and debug test files  

-> TODO : code to fix and clean up  
-> TODO : test the api with json example files  

29/03  
--------------------------
* Tests for jobs, resources and media ongoing  
  
-> TODO : find a way to give absolute paths to a request  
-> TODO : fix errors relative to authorization  
-> TODO : handle exchanges between the api and the database 

